from tables import *


PAD_BLOCK = [[b'\x00', b'\x00', b'\x00', b'\x00'], [b'\x00', b'\x00', b'\x00', b'\x00'],
                 [b'\x00', b'\x00', b'\x00', b'\x00'], [b'\x00', b'\x00', b'\x00', b'\x10']]


def transpose(block):
    return [[block[c][r] for c in range(len(block))] for r in range(len(block[0]))]


def parse_key_file(key_file):
    with open(key_file, 'rb') as f:
        key = []
        byte = f.read(1)
        while byte != b"":
            key.append(byte)
            byte = f.read(1)
    return key


def xor_bytes(list1, list2):
    result = []
    for i in range(4):
        a = list1[i][0]
        b = list2[i][0]
        result.append(bytes([a ^ b]))
    return result


def s_box_byte(byte):
    lower = (byte[0] & 0x0f)
    upper = (byte[0] & 0xf0) >> 4
    return S[upper][lower]


def si_box_byte(byte):
    lower = (byte[0] & 0x0f)
    upper = (byte[0] & 0xf0) >> 4
    return Si[upper][lower]
