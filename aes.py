from util import *
from tables import *
import sys
import argparse as ag

#encryption
'''
Takes in 3 files: key, input file, output file
The key is expected to be length 128 for AES-128, and 256 bits for AES-256
'''


def encrypt(key_file, input_file, output_file, key_type=128):
    blocks = read_input_file(input_file)
    key = parse_key_file(key_file)
    expanded_key = key_expansion(key, key_type)

    round_keys = []
    k = 0
    while k < len(expanded_key):
        round_keys.append(expanded_key[k:k+16])
        k += 16

    num_rounds = 9 if key_type == 128 else 13

    new_blocks = []
    for block in blocks:
        add_round_key(block, round_keys[0])

        for i in range(num_rounds):
            sub_bytes(block)
            block = transpose(block)
            shift_rows(block)
            block = transpose(block)
            mix_columns(block)
            add_round_key(block, round_keys[i + 1])

        sub_bytes(block)
        block = transpose(block)
        shift_rows(block)
        block = transpose(block)
        add_round_key(block, round_keys[-1])
        new_blocks.append(block)
    write_output_file(output_file, new_blocks)


def decrypt(key_file, input_file, output_file, key_type=128):
    blocks = read(input_file)
    key = parse_key_file(key_file)
    expanded_key = key_expansion(key, key_type)

    round_keys = []
    i = 0
    while i < len(expanded_key):
        round_keys.append(expanded_key[i:i+16])
        i += 16

    num_rounds = 9 if key_type == 128 else 13

    new_blocks = []
    for block in blocks:
        add_round_key(block, round_keys[-1])
        for i in range(num_rounds):
            block = transpose(block)
            inv_shift_rows(block)
            block = transpose(block)
            inv_sub_bytes(block)
            add_round_key(block, round_keys[-i-2])
            inv_mix_columns(block)

        block = transpose(block)
        inv_shift_rows(block)
        block = transpose(block)
        inv_sub_bytes(block)
        add_round_key(block, round_keys[0])
        new_blocks.append(block)

    new_blocks[-1] = remove_padding(new_blocks[-1])
    write_output_file(output_file, new_blocks)


def remove_padding(block):
    num_pad_bytes = block[3][3][0]
    for i in range(4):
        for j in range(4):
            if num_pad_bytes > 0:
                block[-(i+1)][-(j+1)] = b''
                num_pad_bytes -= 1

    return block


# cut file up into blocks
def read_input_file(input_file):
    # grab 16 bytes and turn it into a block
    # if you have leftover space, pad the rest of that 4x4 with 0x00's
    # return a list of 4x4 arrays

    with open(input_file, "rb") as f:
        block_list = []
        byte = f.read(1)
        byte_count = 0
        while byte != b"":
            # Do stuff with byte.
            block = []
            # add 4 rows to block
            for i in range(4):
                block_row = []
                # add 4 bytes to row
                for j in range(4):
                    if byte != b"":
                        block_row.append(byte)
                        byte_count += 1
                    else:
                        block_row.append(b'\x00')
                    byte = f.read(1)
                block.append(block_row)
            block_list.append(block)
        num_pad_bytes = 16 - (byte_count % 16)
        if byte_count % 16 == 0:
            block_list.append(PAD_BLOCK)
        if num_pad_bytes != 16:
            block_list[-1][3][3] = bytes([num_pad_bytes])
    return block_list


def read(input_file):
    with open(input_file, "rb") as f:
        block_list = []
        byte = f.read(1)

        while byte != b"":
            block = []
            for i in range(4):
                block_row = []
                for j in range(4):
                    if byte != b"":
                        block_row.append(byte)
                    else:
                        break
                    byte = f.read(1)
                block.append(block_row)
            block_list.append(block)
    return block_list


def write_output_file(output_file, blocks):
    with open(output_file, "wb") as f:
        for i in range(len(blocks)):
            for j in range(4):
                for k in range(4):
                    if blocks[i][j][k] != b"":
                        f.write(bytes(blocks[i][j][k]))


def key_expansion(key, key_type):
    num_bytes = 16 if key_type == 128 else 32
    bytes_required = 176 if key_type == 128 else 240

    expanded_key = [] + key

    rcon_index = 1
    while len(expanded_key) < bytes_required:
        temp = expanded_key[-4:]
        temp = key_schedule_core(temp, rcon_index)
        temp = xor_bytes(temp, expanded_key[-num_bytes:-(num_bytes-4)])
        expanded_key += temp
        rcon_index += 1

        for i in range(3):
            temp = expanded_key[-4:]
            temp = xor_bytes(temp, expanded_key[-num_bytes:-(num_bytes-4)])
            expanded_key += temp

        if key_type == 256 and len(expanded_key) < 240:
            temp = expanded_key[-4:]
            for i in range(4):
                temp[i] = s_box_byte(temp[i])
            temp = xor_bytes(temp, expanded_key[-num_bytes:-(num_bytes - 4)])
            expanded_key += temp

            for i in range(3):
                temp = expanded_key[-4:]
                temp = xor_bytes(temp, expanded_key[-num_bytes:-(num_bytes - 4)])
                expanded_key += temp
    return expanded_key


def key_schedule_core(word, index):
    result = word[1:] + word[:1]
    for i in range(4):
        result[i] = s_box_byte(result[i])
    result[0] = bytes([result[0][0] ^ Rcon[index][0]])
    return result


# takes file and cuts it up into blocks
# represents blocks into 4x4 array
# run each block through the sub_bytes, shift_rows, mix_columns, add_round_key

def sub_bytes(block):
    for i in range(4):
        for j in range(4):
            block[i][j] = s_box_byte(block[i][j])


def inv_sub_bytes(block):
    for i in range(4):
        for j in range(4):
            block[i][j] = si_box_byte(block[i][j])


# for each row, shift left i # bytes, where i is the row you're in
def shift_rows(block):
    for i in range(4):
        # grab this row
        row = block[i]
        # shift
        # -i wraps around if needed
        block[i] = row[i:] + row[:i]


def inv_shift_rows(block):
    for i in range(4):
        # grab this row
        row = block[i]
        # shift
        # -i wraps around if needed
        block[i] = row[-i:] + row[:-i]


# Rijndael's MixColumns based on the Wikipedia page's example (which was in C)
# mixes the bytes by column NOT row (hence the name)
def mix_columns(block):
    for i in range(4):
        b0 = block[i][0][0]
        b1 = block[i][1][0]
        b2 = block[i][2][0]
        b3 = block[i][3][0]
        block[i][0] = bytes([T2[b0][0] ^ T3[b1][0] ^ b2 ^ b3])
        block[i][1] = bytes([b0 ^ T2[b1][0] ^ T3[b2][0] ^ b3])
        block[i][2] = bytes([b0 ^ b1 ^ T2[b2][0] ^ T3[b3][0]])
        block[i][3] = bytes([T3[b0][0] ^ b1 ^ b2 ^ T2[b3][0]])


def inv_mix_columns(block):
    for i in range(4):
        b0 = block[i][0][0]
        b1 = block[i][1][0]
        b2 = block[i][2][0]
        b3 = block[i][3][0]
        block[i][0] = bytes([T14[b0][0] ^ T11[b1][0] ^ T13[b2][0] ^ T9[b3][0]])
        block[i][1] = bytes([T9[b0][0] ^ T14[b1][0] ^ T11[b2][0] ^ T13[b3][0]])
        block[i][2] = bytes([T13[b0][0] ^ T9[b1][0] ^ T14[b2][0] ^ T11[b3][0]])
        block[i][3] = bytes([T11[b0][0] ^ T13[b1][0] ^ T9[b2][0] ^ T14[b3][0]])


# XOR state with 128 bit round key from original key K with recursion
# need the original key k, pass it in?
def add_round_key(block, round_key):
    for i in range(4):
        block[i] = xor_bytes(block[i], round_key[i*4:i*4+4])


def main(key_size, key_file, input_file, output_file, mode):
    if mode == "encrypt":
        encrypt(key_file, input_file, output_file, key_size)
    elif mode == "decrypt":
        decrypt(key_file, input_file, output_file, key_size)
    else:
        print("Quitting. Incorrect value entered")


parser = ag.ArgumentParser()
parser.add_argument('--keysize', type=int, help='enter the key size, 128 or 256')
parser.add_argument('--keyfile', type=str, help='enter they file containing the key')
parser.add_argument('--inputfile', type=str, help='enter the input file that needs to be encrypted/decrypted')
parser.add_argument('--outputfile', type=str, help='enter the output file to write the encrypted/decrypted file into')
parser.add_argument('--mode', type=str, help='enter \'encrypt\' or \'decrypt\' for the mode')
args = parser.parse_args()
main(args.keysize, args.keyfile, args.inputfile, args.outputfile, args.mode)
