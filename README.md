# CS 361 AES Encryption

Running the Code:

python3 --keysize 128 --keyfile key_128 --inputfile in_rand --outputfile out_enc_128 --mode encrypt





Implementation of AES encryption, both 128 and 256 bit versions, using ECB mode.

Encrypt:
1.	read_input_file:
- We read in one byte at a time. While we still have characters left, we create a block
- Add 4 rows to the block, and 4 bytes to each row. 
- If we run out of characters, then pad it. 
- Then calculate the number of bytes we added. If it’s not a multiple of 16, bad our block list with that many blocks. 
2.	Parse_key_file
- Put each byte into an array
3.	Key_expansion:
- Grab the right number of bytes for the key, and for the output. 
- While we haven’t finished expanding the key
- The first 4 bytes are 1 word, 2nd four bytes are another word, etc…
- We exclusive-OR t with the four-byte block n bytes before the new expanded key. 
This becomes the next 4 bytes in the expanded key
    -if we are processing a 256-bit key:
    -We assign the value of the previous 4 bytes in the expanded key to t
    -We run each of the 4 bytes in t through Rijndael's S-box
    -We exclusive-OR t with the 4-byte block n bytes before the new expanded key.
    This becomes the next 4 bytes in the expanded key.
4.	Take in the expanded key and put it into an array with 16 bytes each
5.  For each block, we first add_round key to start off our process. Then we go through the following process 
	num_rounds number of times (determined by the key size): sub_bytes, transpose, shift_rows, transpose, mix_columns,
	add_round_key. This was given by the assignment sheet. 
6.	Sub_bytes
- The block has 16 bytes in it, placed in a square. Use the table to replace the index with the right element from the table
7.	Transpose
- 	Returns a transposed view of the multi-dimensional matrix.
8.	Shift_rows
- Shift each row left by i bytes and wrap the leftovers back around the block
9.	Mix_columns
- 	Combines the four bytes in each column of the array 
10.	Add_round_key
- 	For each column, xor the bytes with the round key


Decrypt:
1.	read_input_file:
- We read in one byte at a time. While we still have characters left, we create a block
- Add 4 rows to the block, and 4 bytes to each row. 
- If we run out of characters, then pad it. 
- Then calculate the number of bytes we added. If it’s not a multiple of 16, bad our block list with that many blocks. 
2.	Parse_key_file
- Put each byte into an array
3.	Key_expansion:
- Grab the right number of bytes for the key, and for the output. 
- While we haven’t finished expanding the key
- The first 4 bytes are 1 word, 2nd four bytes are another word, etc…
- We exclusive-OR t with the four-byte block n bytes before the new expanded key. 
This becomes the next 4 bytes in the expanded key
    -if we are processing a 256-bit key:
    -We assign the value of the previous 4 bytes in the expanded key to t
    -We run each of the 4 bytes in t through Rijndael's S-box
    -We exclusive-OR t with the 4-byte block n bytes before the new expanded key.
    This becomes the next 4 bytes in the expanded key.
4. Take the expanded key and put it into an array with 16 bytes each.
5.  For each block, we first add_round key to start off our process. Then we go through the following process:
transpose our current block, inverse shift the rows of that block, inverse sub bytes, add the round key, and inverse mix columns.
6.inv_sub_bytes
-The block has 16 bytes in it, placed in a square. Use the table to replace the index with the right element from the table
7.	Transpose
- 	Returns a transposed view of the multi-dimensional matrix.
8.	inv_shift_rows
- Shift each row left by -i bytes and wrap the leftovers back around the block
9.	Add_round_key
- 	For each column, xor the bytes with the round key
10. inv_mix_columns
- 	Combines the four bytes in each column of the array
